package z2z;

public class Juke {
    private int numPatrons;

    public Juke() {
        this.numPatrons = 0;
    }

    /**
     * @return the numPatrons
     */
    public int getNumPatrons() {
        return numPatrons;
    }

    /**
     * @param numPatrons
     *            the numPatrons to set
     */
    public void setNumPatrons(int numPatrons) {
        this.numPatrons = numPatrons;
    }

    public static void main(String[] args) {
        Juke juke = new Juke();
        juke.getNumPatrons();
    }

}
