/**
 * 
 */
package z2z;

import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

/**
 * @author Ruben
 *
 */
public class JukeTest {
    private Juke juke;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        juke = new Juke();
    }

    /**
     * Test method for {@link z2z.Juke#getNumPatrons()}.
     */
    @Test
    public void testGetNumPatrons() {
        Assert.assertEquals(0, juke.getNumPatrons());
    }

    /**
     * Test method for {@link z2z.Juke#setNumPatrons(int)}.
     */
    @Test
    public void testSetNumPatrons() {
        juke.setNumPatrons(5);
        Assert.assertEquals(5, juke.getNumPatrons());
    }

}
